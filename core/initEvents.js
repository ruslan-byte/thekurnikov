import { music } from "./music.js";

function clickEvent() {
	music.init();
	this.removeEventListener('click', clickEvent);
	this.removeEventListener('focus', clickEvent);
}

document.querySelector('body').addEventListener('click', clickEvent);
document.querySelector('body').addEventListener('focus', clickEvent);